#pragma once

#include <assert.h>
#include <mysql/mysql.h>
#include <stdint.h>
#include <vector>
#include <map>

#include "field.h"

//该类封装的查询的结果集，利用RAII手法管理结果集资源
class QueryResult
{
public:
	typedef std::map<uint32_t, std::string> FieldNames;

	QueryResult(MYSQL_RES* result, uint64_t rowCount, uint32_t fieldCount);
	virtual ~QueryResult();

	virtual bool NextRow();

	uint32_t GetField_idx(const std::string& name) const
	{
		for (FieldNames::const_iterator iter = GetFieldNames().begin(); iter != GetFieldNames().end(); ++iter)
		{
			if (iter->second == name)
				return iter->first;
		}

		assert(false && "unknown field name");
		return uint32_t(-1);
	}

	Field *Fetch() const { return m_pCurrentRow; }

	const Field & operator [] (int index) const
	{
		return m_pCurrentRow[index];
	}

	const Field & operator [] (const std::string &name) const
	{
		return m_pCurrentRow[GetField_idx(name)];
	}

	uint32_t GetFieldCount() const { return m_FieldCount; }
	uint64_t GetRowCount() const { return m_RowCount; }
	FieldNames const& GetFieldNames() const { return m_mapFieldNames; }

	vector<string> const& GetNames() const { return m_vtFieldNames; }

private:
	enum Field::DataTypes ConvertNativeType(enum_field_types mysqlType) const;
public:
	void EndQuery(); 
protected:
	Field				*m_pCurrentRow;		//存储结果集的每一行的行信息,根据构造函数的参数fieldCount申请一块堆空间
	uint32_t			m_FieldCount;		//结果集一共有多少列
	uint64_t			m_RowCount;			//结果集共包含多少行
	FieldNames			m_mapFieldNames;	//
	std::vector<string>	m_vtFieldNames;

	MYSQL_RES*			m_result;
};