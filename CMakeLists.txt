PROJECT (MYSERVER)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g -Wall -O0 -Wno-unused-variable -pthread")

#添加非标准的共享库的搜索路径
link_directories(
    ${PROJECT_SOURCE_DIR}/lib
	/usr/lib64/mysql/
)
#在指定路径中搜索库，路径包括环境变量的path中的所有路径
#FIND_LIBRARY(<VAR> name1 path1 path2 ...) 
find_library(MYSQL_LIB libmysqlclient.so /usr/lib64/mysql/)
IF (NOT MYSQL_LIB)
    MESSAGE(FATAL_ERROR "mysqlclient not found")
ENDIF(NOT MYSQL_LIB)

SET(net_srcs 
base/timestamp.cpp
base/logging.cpp
base/logstream.cpp
net/Buffer.cc
net/EventLoop.cc
net/Channel.cc
net/Poller.cc
net/Poller/DefaultPoller.cc
net/Poller/EPollPoller.cc
net/Poller/PollPoller.cc
net/Timer.cc
net/TimerQueue.cc
net/EventLoopThread.cc
net/SocketsOps.cc
net/InetAddress.cc
net/Socket.cc
net/Acceptor.cc
net/TcpConnection.cc
net/TcpServer.cc
net/Connector.cc
net/TcpClient.cc
net/EventLoopThreadPool.cc
net/protocolstream.cc
)

SET(json_srcs
jsoncpp-0.5.0/json_reader.cpp
jsoncpp-0.5.0/json_value.cpp
jsoncpp-0.5.0/json_writer.cpp
)

SET(sql_srcs
database/field.cpp
database/queryresult.cpp
database/DatabaseMysql.cpp
mysql/tasklist.cpp
mysql/MysqlThrd.cpp
mysql/MysqlThrdMgr.cpp
mysql/MysqlManager.cpp
)

SET(chat_server
chatserversrc/ClientSession.cpp
chatserversrc/TcpSession.cpp
chatserversrc/IMServer.cpp
chatserversrc/UserManager.cpp
chatserversrc/MsgCacheManager.cpp
chatserversrc/main.cpp
)

SET(file_server
fileserversrc/FileManager.cpp
fileserversrc/FileServer.cpp
fileserversrc/FileSession.cpp
fileserversrc/MD5.cpp
fileserversrc/TcpSession.cpp
fileserversrc/main.cpp
)

MESSAGE(STATUS "This is BINARY dir" ${PROJECT_BINARY_DIR})
MESSAGE(STATUS "This is SOURCE dir" ${PROJECT_SOURCE_DIR})
ADD_EXECUTABLE(chatserver ${net_srcs} ${chat_server} ${json_srcs} ${sql_srcs})
ADD_EXECUTABLE(fileserver ${net_srcs} ${file_server} ${json_srcs} ${sql_srcs})
#光包含库目录是没用的，还必须使用TARGET_LINK_LIBRARIES链接该库
#TARGET_LINK_LIBRARIES(target library1  <debug | optimized> library2...)
TARGET_LINK_LIBRARIES(chatserver mysqlclient)
TARGET_LINK_LIBRARIES(fileserver mysqlclient)
#ADD_EXECUTABLE(client ${net_srcs} ${net_test2})