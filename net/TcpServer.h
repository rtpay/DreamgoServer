#ifndef NET_TCPSERVER_H
#define NET_TCPSERVER_H

#include "Callbacks.h"
#include "TcpConnection.h"

#include <map>
#include <memory>

class Acceptor;
class EventLoop;

class TcpServer
{
public:
	enum Option
	{
		kNoReusePort,
		kReusePort,
	};

	TcpServer(EventLoop* loop, const InetAddress& listenAddr, const std::string& nameArg, Option option = kReusePort);
	~TcpServer();  // force out-line dtor, for scoped_ptr members.
	
	/// Starts the server if it's not listenning.
	///
	/// It's harmless to call it multiple times.
	/// Thread safe.
	void start();
	
	///set connection callback.
	///not thread safe.
	void setConnCallback(ConnectionCallback cb){ m_connCallback = cb; }
	///set message callback.
	///not thread safe.
	void setMsgCallback(MessageCallback cb){ m_msgCallback = cb; }
	/// Set write complete callback.
	/// Not thread safe.
	void setWriteCompleteCallback(const WriteCompleteCallback& cb)
	{
		m_writeCompleteCallback = cb;
	}
private:

	///Not thread safe, but in loop
	void newConnection(int sockfd, const InetAddress& peerAddr);
	void removeConnection(const TcpConnectionPtr& conn);
	void removeConnectionInLoop(const TcpConnectionPtr& conn);
	typedef std::map<std::string, TcpConnectionPtr> TcpConnectionMap;
	
	EventLoop*					m_pLoop;		//the acceptor loop
	const std::string			m_strName;
	const std::string			m_strHostPort;
	std::unique_ptr<Acceptor>	m_pAcceptor;
	ConnectionCallback			m_connCallback;
	MessageCallback				m_msgCallback;
	WriteCompleteCallback       m_writeCompleteCallback;
	bool						m_bStarted;
	int							m_nextConnId;		//always in loop thread
	TcpConnectionMap			m_mapConn;
	
};
#endif //NET_TCPSERVER_H