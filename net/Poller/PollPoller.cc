#include "PollPoller.h"

#include "../Channel.h"
#include "../../base/logging.h"
#include "../../base/timestamp.h"

#include <poll.h>

PollPoller::PollPoller(EventLoop* loop):
	Poller(loop)
{

}

PollPoller::~PollPoller()
{

}

Timestamp PollPoller::poll(int timeoutMs, ChannelList * activeChannels)
{
	// XXX pollfds_ shouldn't change
	int numEvents = ::poll(&*pollfds_.begin(), pollfds_.size(), timeoutMs);
	int savedErrno = errno;
	Timestamp now(Timestamp::now());
	if (numEvents > 0)
	{
		LOG_TRACE << numEvents << " events happened";
		fillActiveChannels(numEvents, activeChannels);
	}
	else if (numEvents == 0)
	{
		LOG_TRACE << " nothing happened";
	}
	else
	{
		if (savedErrno != EINTR)
		{
			errno = savedErrno;
			LOG_SYSERR << "PollPoller::poll()";
		}
	}
	return now;
}

void PollPoller::updateChannel(Channel * channel)
{
	assertInLoopThread();
	LOG_TRACE << "updateChannel,fd = " << channel->fd() << " events = " << channel->events();
	if (channel->index() < 0)
	{
		assert(channels_.find(channel->fd()) == channels_.end());
		//new fd,add to poll list
		struct pollfd pfd;
		pfd.fd = channel->fd();
		pfd.events = static_cast<short>( channel->events() );
		pfd.revents = 0;
		pollfds_.push_back(pfd);
		//update channel poll index
		int index = static_cast<int>(pollfds_.size()-1);
		channel->set_index(index);
		//update Poller fd list
		channels_[pfd.fd] = channel;
	}
	else
	{
		//检验map中channel的合法性
		assert(channels_.find(channel->fd()) != channels_.end());
		assert(channels_[channel->fd()] == channel);
		int idx = channel->index();
		//检查channel中index标识合法性
		assert( (0 <= idx) && (idx < static_cast<int>(pollfds_.size())));
		struct pollfd& pfd = pollfds_[idx];
		//检查struct poll 结构体fd的合法性
		assert(pfd.fd == channel->fd() || pfd.fd == -channel->fd() - 1);
		//改变关注事件
		pfd.events = static_cast<short>(channel->events());
		pfd.fd = channel->fd();
		pfd.revents = 0;
		if (channel->isNoneEvent())
		{
			// ignore this pollfd
			pfd.fd = -channel->fd() - 1;
		}
	}
}

void PollPoller::removeChannel(Channel * channel)
{
	assertInLoopThread();
	LOG_TRACE << "removeChannel,fd = " << channel->fd() << " events = " << channel->events();
	//检查channel在channels_中的合法性
	assert(channels_.find(channel->fd()) != channels_.end());
	assert(channels_[channel->fd()] == channel);
	assert(channel->isNoneEvent());
	size_t n = channels_.erase(channel->fd());
	assert(n == 1); (void)n;

	int idx = channel->index();
	//检查channel中index标识合法性
	assert((0 <= idx) && (idx < static_cast<int>(pollfds_.size())));
	const struct pollfd& pfd = pollfds_[idx];
	assert(pfd.fd == -channel->fd() - 1 && pfd.events == channel->events());
	if (static_cast<size_t>(idx) == pollfds_.size() - 1)
	{
		pollfds_.pop_back();
	}
	else
	{
		int channelAtEnd = pollfds_.size() - 1;
		iter_swap(pollfds_.begin() + idx, pollfds_.end() - 1);
		if (channelAtEnd < 0)
		{
			channelAtEnd = -channelAtEnd - 1;		//还原fd
		}
		//调整位置以后需要改变对应channel中保存的poll数组中的idx
		channels_[channelAtEnd]->set_index(idx);
		pollfds_.pop_back();
	}
}

void PollPoller::fillActiveChannels(int numEvents, ChannelList * activeChannels) const
{
	for (PollFdList::const_iterator pfd = pollfds_.begin();
		pfd != pollfds_.end() && numEvents > 0; ++pfd)
	{
		if (pfd->revents > 0)
		{
			ChannelMap::const_iterator it = channels_.find(pfd->fd);
			assert(it != channels_.end());
			Channel* ch = it->second;
			assert(ch->fd() == pfd->fd);
			ch->set_revents(pfd->revents);
			activeChannels->push_back(ch);
			numEvents--;
		}
	}
}


