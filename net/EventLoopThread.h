#ifndef NET_EVENTLOOPTHREAD_H
#define NET_EVENTLOOPTHREAD_H

#include <condition_variable>
#include <mutex>
#include <thread>
#include <memory>

class EventLoop;

class EventLoopThread
{
public:
	typedef std::function<void(EventLoop*)> ThreadInitCallback;
	EventLoopThread(const ThreadInitCallback& cb = ThreadInitCallback(),
		const std::string& name = std::string());
	~EventLoopThread();
	const EventLoopThread& operator=(const EventLoopThread&) = delete;
	EventLoopThread(const EventLoopThread&) = delete;
	
	EventLoop* startLoop();
private:
	void threadFunc();
	
	EventLoop*						loop_;
	bool							exiting_;
	std::unique_ptr<std::thread>	thread_;
	std::mutex						mutex_;
	std::condition_variable			cond_;
	ThreadInitCallback				callback_;
};

#endif  //  NET_EVENTLOOPTHREAD_H