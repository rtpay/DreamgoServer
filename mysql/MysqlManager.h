/******************************************************************
Copyright (C) 2018 - All Rights Reserved by
文 件 名 : MysqlManager.h --- CMysqlManager
编写日期 : 2018/12/7
说 明    : 该类用于启动聊天服务程序时，检查数据库是否连接正常，对应的数据库数据表是否存在
历史纪录 :
<作者>    <日期>        <版本>        <内容>
*******************************************************************/
#pragma once

#include <memory>
#include <map>
#include <vector>

#include "../database/DatabaseMysql.h"

#define MAXCMDLEN 8291

struct STableField
{
	STableField() {}
	STableField(std::string strName, std::string strType, std::string strIndex) :
		m_strName(strName),
		m_strType(strType),
		m_strDesc(strIndex)
	{
	}
	std::string m_strName;
	std::string m_strType;
	std::string m_strDesc;
};

struct STableInfo
{
	STableInfo() {}
	STableInfo(std::string strName)
		:m_strName(strName)
	{
	}
	std::string m_strName;
	std::map<std::string, STableField> m_mapField;
	std::string m_strKeyString;
};

class CMysqlManager
{
public:
	CMysqlManager(void);
	~CMysqlManager(void);

public:
	bool Init(const char* host, const char* user, const char* pwd, const char* dbname);

	std::string GetHost() { return m_strHost; }
	std::string GetUser() { return m_strUser; }
	std::string GetPwd() { return m_strPassword; }
	std::string GetDBName() { return m_strDataBase; }
	std::string GetCharSet() { return m_strCharactSet; }
private:
	bool _IsDBExit();
	bool _CreateDB();
	bool _CheckTable(const STableInfo& table);
	bool _CreateTable(const STableInfo& table);
protected:
	std::shared_ptr<CDatabaseMysql>  m_poConn;

	std::string m_strHost;
	std::string m_strUser;
	std::string m_strPassword;
	std::string m_strDataBase;
	std::string m_strCharactSet;

	std::vector<STableInfo> m_vecTableInfo;
};

