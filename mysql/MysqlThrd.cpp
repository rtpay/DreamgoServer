#include "MysqlThrd.h"
#include "../base/logging.h"

CMysqlThrd::CMysqlThrd(void):
	m_bTerminate(false),
	m_bStart(false),
	m_poConn(nullptr)
{
	
}


CMysqlThrd::~CMysqlThrd(void)
{
	if (m_poConn != nullptr)
		delete m_poConn;
}

void CMysqlThrd::Run()
{
	_MainLoop();
	_Uninit();

	if (nullptr != m_pThread)
	{
		m_pThread->join();
	}
}

//连接数据库，子线程开始执行loop
bool CMysqlThrd::Start(const string & host, const string & user, const string & pwd, const string & dbname)
{
	m_poConn = new CDatabaseMysql();		//FIXME:在哪释放的空间？
	if (nullptr == m_poConn)
	{
		LOG_FATAL << "CMysqlThrd::Start, Cannot open database";
		return false;
	}

	if (m_poConn->Initialize(host, user, pwd, dbname) == false)
	{
		return false;
	}

	return _Init();
}

void CMysqlThrd::Stop()
{
	if (m_bTerminate)
	{
		return;
	}
	m_bTerminate = true;
	m_pThread->join();
}

//初始化启动线程，等子线程实际运行起来以后，主线程继续运行
bool CMysqlThrd::_Init()
{
	if (m_bStart)
	{
		return true;
	}

	//启动线程
	std::make_shared<std::thread>(std::bind(&CMysqlThrd::_MainLoop, this));
	
	{
		std::unique_lock<std::mutex> lock(mutex_);
		while (false == m_bStart)
		{
			cond_.wait(lock);
		}
	}

	return false;
}

//子线程主循环
void CMysqlThrd::_MainLoop()
{
	m_bStart = true;

	{
		std::unique_lock<std::mutex> lock(mutex_);
		cond_.notify_all();		//通知主线程继续运行
	}

	IMysqlTask* poTask;

	while (!m_bTerminate)
	{
		if (nullptr != (poTask = m_oTask.Pop()))
		{
			poTask->Execute(m_poConn);
			m_oReplyTask.Push(poTask);
			continue;
		}

		this_thread::sleep_for(chrono::milliseconds(1000));
	}
}

void CMysqlThrd::_Uninit()
{
}
