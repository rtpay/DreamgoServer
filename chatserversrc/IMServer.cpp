#include "../net/InetAddress.h"
#include "../base/logging.h"
#include "../base/singleton.h"
#include "IMServer.h"
#include "ClientSession.h"
#include "UserManager.h"

#include <sstream>

using namespace placeholders;

//Server初始化，开启侦听
bool IMServer::Init(const char* ip, short port, EventLoop* loop)
{
	InetAddress addr(ip, port);
	m_server = std::make_shared<TcpServer>(loop, addr, "ZRJ-MYIMSERVER", TcpServer::kReusePort);
	m_server->setConnCallback(std::bind(&IMServer::OnConnection, this, _1));
	//启动侦听
	m_server->start();
	return true;
}

bool IMServer::IsUserSessionExsit(int32_t userid)
{
	std::lock_guard<std::mutex> guard(m_sessionMutex);
	for (const auto& iter : m_mapSessions)
	{
		if (iter.second->GetUserId() == userid)
		{
			return true;
		}
	}
	return false;
}

bool IMServer::GetSessionByUserId(std::shared_ptr<ClientSession>& session, int32_t userid)
{
	std::lock_guard<std::mutex> lock(m_sessionMutex);
	for (const auto& iter : m_mapSessions)
	{
		if (userid == iter.second->GetUserId())
		{
			session = iter.second;
			return true;
		}
	}
	return false;
}

//这个函数会在不同子线程之间调用
void IMServer::OnConnection(TcpConnectionPtr conn)
{
	//std::stringstream ss;
	//ss << this_thread::get_id();
	//LOG_INFO << "IMServer::OnClose: " << ss.str();
	if (conn->connected())
	{
		LOG_INFO << "client connected:" << conn->peerAddress().toHostPort();
		++m_baseUserId;
		ClientSessionPtr spSession = std::make_shared<ClientSession>(conn);
		conn->setMsgCallback(std::bind(&ClientSession::OnRead, spSession.get(), _1,_2,_3));
		std::string nameConn = conn->name();

		{	
			std::lock_guard<std::mutex> lock(m_sessionMutex);
			m_mapSessions[nameConn] = spSession;
		}
	}
	else
	{
		OnClose(conn);
	}
}

//这个函数会在不同子线程之间调用
void IMServer::OnClose(const TcpConnectionPtr& conn)
{
	//std::stringstream ss;
	//ss << this_thread::get_id();
	//LOG_INFO << "IMServer::OnClose: " << ss.str();
	std::string nameConn = conn->name();
	{
		std::lock_guard<std::mutex> guard(m_sessionMutex);
		const std::map<std::string, ClientSessionPtr>::const_iterator& iter = m_mapSessions.find(nameConn);
		if (iter != m_mapSessions.end())
		{
			//遍历其在线好友，给其好友推送离线消息
			std::list<User> friends;
			int32_t offlineUserId = iter->second->GetUserId();
			UserManager& userManager = Singleton<UserManager>::Instance();
			userManager.GetFriendInfoByUserId(offlineUserId, friends);
			for (const auto& user : friends)
			{
				for (auto& pair : m_mapSessions)
				{
					//如果该好友在线则推送通知
					if (user.userid == pair.second->GetUserId())
					{
						pair.second->SendUserStatusChangeMsg(offlineUserId, 2);
						break;
					}
				}
			}
			//停掉该Session的掉线检测
			(iter->second)->DisableHeartbeatCheck();
			//用户下线
			m_mapSessions.erase(iter);
		}
		else
		{
			LOG_ERROR << "Session is not Exit!";
		}
		
	}
	LOG_INFO << "client disconnected: " << conn->peerAddress().toHostPort();
	LOG_INFO << "current online user count: " << m_mapSessions.size();
}