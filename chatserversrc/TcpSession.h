/******************************************************************
Copyright (C) 2018 - All Rights Reserved by
文 件 名 : TcpSession.h --- TcpSession
编写日期 : 2018/12/7
说 明    : 负责传输层数据加上包头信息然后发送，每一个连接都有唯一一个TcpSession
历史纪录 :
<作者>    <日期>        <版本>        <内容>
*******************************************************************/
#pragma once

#include <memory>
#include "../net/TcpConnection.h"
class TcpSession
{
public:
	TcpSession(const std::weak_ptr<TcpConnection>& tmpconn);
	~TcpSession();

	TcpSession(const TcpSession& rhs) = delete;
	TcpSession& operator=(const TcpSession& rhs) = delete;

	TcpConnectionPtr GetConnectionPtr()
	{ 
		if (m_tmpConn.expired())
		{
			return NULL;
		}

		return m_tmpConn.lock(); 
	}

	void Send(const std::string& buf);
	void Send(const char* p, int length);
protected:
	//TcpSession引用TcpConnection类必须是弱指针，因为TcpConnection可能会因网络出错自己销毁，此时TcpSession应该也要销毁
	std::weak_ptr<TcpConnection>    m_tmpConn;
};